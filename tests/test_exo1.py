#!/usr/bin/env python
# encoding: utf-8

import unittest
from random import *
from math import *
from projet import distance_euclidienne



x=[randint(-10,10),randint(-10,10)]
y=[randint(-10,10),randint(-10,10)]
z=[randint(-10,10),randint(-10,10)]

class test_distance_euclidienne(unittest.TestCase):
    def test_distance_non_nulle(self):
        self.assertEqual(distance_euclidienne(x,x),0)
    def test_toujours_positif(self):
        self.assertGreaterEqual(distance_euclidienne(x,y),0)
    def test_reciprocite(self):
        self.assertEqual(distance_euclidienne(x,y),distance_euclidienne(y,x))
    def test_trois_points(self):
        self.assertLessEqual(distance_euclidienne(x,z),distance_euclidienne(x,y)+distance_euclidienne(y,z))
    def test_zero_un(self):
        self.assertAlmostEqual(distance_euclidienne([0,1],[1,0]),sqrt(2))
