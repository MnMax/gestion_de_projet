#!/usr/bin/env python
# encoding: utf-8

import unittest
from projet import calcul_taux_satisfaction
from random import *


monAntenne =[10,10]


class test_satisfaction(unittest.TestCase):
    def test_gens_nul(self):
        self.assertEqual(calcul_taux_satisfaction([1,1,0,1],monAntenne),0)
    def test_gens_negatif(self):
        self.assertEqual(calcul_taux_satisfaction([1,1,-5,1],monAntenne),0)
    #def meme_distance_mais_plus_interet
