#!/usr/bin/env python
# encoding: utf-8

import unittest
from projet import calcul_qualite_signal



class test_calcul_qualite(unittest.TestCase):
    def test_inf_100(self):
        self.assertEqual(calcul_qualite_signal(-110.0),0)
    def test_inf_80(self):
        self.assertEqual(calcul_qualite_signal(-82.0),1)
    def test_inf_60(self):
        self.assertEqual(calcul_qualite_signal(-62.0),2)
    def test_inf_40(self):
        self.assertEqual(calcul_qualite_signal(-42.0),3)
    def test_inf_20(self):
        self.assertEqual(calcul_qualite_signal(-22.0),4)
    def test_inf_10(self):
        self.assertEqual(calcul_qualite_signal(-12.0),5)
