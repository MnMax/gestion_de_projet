#!/usr/bin/env python
# encoding: utf-8

import unittest
from projet import calcul_puissance_recue



class test_antenne(unittest.TestCase):
    def test_pied_antenne(self):
        self.assertEqual(calcul_puissance_recue([0,0],[0,0]),0)
    def test_distance_inferieure_un(self):
        self.assertEqual(calcul_puissance_recue([0,0],[0.5,0.5]),0)
    def test_dix(self):
        self.assertEqual(calcul_puissance_recue([0,0],[0,10]),-20)
    def test_decroissance(self):
        self.assertLess(calcul_puissance_recue([0,0],[0,20]),calcul_puissance_recue([0,0],[0,10]))
