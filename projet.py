#!/usr/bin/env python
# encoding: utf-8

from math import *


def distance_euclidienne(a,b):
    return sqrt((b[0]-a[0])**2+(b[1]-a[1])**2)

def calcul_puissance_recue(position,antenne):
    if position == antenne == [0,0]:
        return 0
    if distance_euclidienne(position,antenne) < 1:
        return 0
    return -20*log10(distance_euclidienne(position,antenne))

def calcul_qualite_signal(qualite):
    if qualite < -100:
        return 0
    if qualite < -80:
        return 1
    if qualite < -60:
        return 2
    if qualite < -40:
        return 3
    if qualite < -20:
        return 4
    if qualite < -10:
        return 5

def calcul_taux_satisfaction(domicile,antenne):
    qualite = calcul_qualite_signal(calcul_puissance_recue([domicile[0],domicile[1]],antenne))
    nbrgens = domicile[2]
    interet = domicile[3]

    if nbrgens <= 0:
        return 0
    if interet == 0:
        return 0

    interetglobal = nbrgens*interet

    if interetglobal < 0:
        return abs(interetglobal*distance)
    if interetglobal > 0:
        return interetglobal/distance
